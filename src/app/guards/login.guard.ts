import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { map, Observable, tap } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate, CanLoad {
  constructor(private authService: AuthService, private router: Router){}
  canActivate() : Observable<boolean> | boolean {
    return !this.authService.getUserByToken().pipe(
      tap( valid => {
        if( valid ) {
          this.router.navigateByUrl('/dashboard')
        }
      })
    );
  }
  canLoad():  Observable<boolean> | boolean {
    return !this.authService.getUserByToken().pipe(
      tap( valid => {
        if( valid ) {
          this.router.navigateByUrl('/dashboard')
        }
      })
    );
  }
}
