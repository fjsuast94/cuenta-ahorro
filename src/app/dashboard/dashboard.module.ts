import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { MainComponent } from './pages/main/main.component';
import { ClientComponent } from './pages/client/client.component';
import { AccountComponent } from './pages/account/account.component';
import { HomeComponent } from './pages/home/home.component';
import { ClientCreateComponent } from './pages/client-create/client-create.component';
import { AccountCreateComponent } from './pages/account-create/account-create.component';
import { AccountTransactionComponent } from './pages/account-transaction/account-transaction.component';
import { AccountTransactionHistorialComponent } from './pages/account-transaction-historial/account-transaction-historial.component';


@NgModule({
  declarations: [
    MainComponent,
    ClientComponent,
    AccountComponent,
    HomeComponent,
    ClientCreateComponent,
    AccountCreateComponent,
    AccountTransactionComponent,
    AccountTransactionHistorialComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    RouterModule,
    ReactiveFormsModule,
  ]
})
export class DashboardModule { }
