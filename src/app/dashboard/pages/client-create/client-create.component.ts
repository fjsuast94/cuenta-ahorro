import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Client } from 'src/app/interfaces/client';
import { BankService } from 'src/app/services/bank.service';
import { v4 as uuidv4 } from 'uuid';
import Swal from "sweetalert2";

@Component({
  selector: 'app-client-create',
  templateUrl: './client-create.component.html',
  styles: [
  ]
})
export class ClientCreateComponent implements OnInit {

  clientForm: FormGroup = this.fb.group({
    nombre: ['', [Validators.required]],
    direccion: ['', [Validators.required]],
    edad: ['', [Validators.required, Validators.min(19), Validators.max(100)]],
    genero: ['', [Validators.required]],
  });

  constructor( private fb: FormBuilder, private bankService : BankService) { }

  ngOnInit(): void {}

  isValidFiel(field: string) {
    return this.clientForm.controls[field].errors && this.clientForm.controls[field].touched;
  }
  // get controls from form builder
  get f () {
    return this.clientForm.controls;  
  }


  onSubmit() {
    const {  direccion, edad, genero, nombre } = this.clientForm.value;

    const clients : Client = {
      uuid: Math.floor(Math.random() * 200),
      nombre,
      direccion,
      edad,
      genero,
    }
    this.bankService.createClient(clients);
    this.clientForm.reset()
    Swal.fire({
      icon: 'success',
      title: 'Se ha guardado al cliente correctamente',
      showConfirmButton: false,
      timer: 1500
    })
  }

}
