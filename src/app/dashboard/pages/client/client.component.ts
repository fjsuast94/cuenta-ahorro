import { Component, OnInit, OnDestroy } from '@angular/core';
import { Client } from 'src/app/interfaces/client';
import { BankService } from 'src/app/services/bank.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styles: [
  ]
})
export class ClientComponent implements OnInit {
  private scripts = ['./assets/js/vendors.js', './assets/js/app.js'];

  clients: Array<Client> = [];

  constructor(private bank : BankService) {
    this.clients = this.bank.readClient()
  }

  ngOnInit(): void {
    //this.addScriptJs(this.scripts)
  }

  ngOnDestroy(): void {
    //this.deleteScripJs(this.scripts)
  }


   addScriptJs(scripts :Array<string> = []){
    scripts.forEach(script => {
      const tag = document.createElement('script');
      tag.setAttribute('src', script);
      tag.setAttribute('type', 'text/javascript');
      document.body.appendChild(tag);
    });
  };

   deleteScripJs(scripts :Array<string> = []){
    scripts.forEach(script => {
      const tag = document.querySelector("script[src='" + script + "']");
      if (tag) {
        tag.remove();
      }
    });
  };

}
