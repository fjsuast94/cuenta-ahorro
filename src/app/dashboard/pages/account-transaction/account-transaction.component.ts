import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BankService } from 'src/app/services/bank.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-account-transaction',
  templateUrl: './account-transaction.component.html',
  styles: [
  ]
})
export class AccountTransactionComponent implements OnInit {

  formTransaction = this.fb.group({
    monto: [0, [Validators.required, Validators.min(0)]],
    numeroCuenta: ['', [Validators.required ]],
    terminal: ['', [Validators.required ]],
    tipo: ['', [Validators.required ]],
  })

  constructor( private bankService: BankService, private route: ActivatedRoute, private fb: FormBuilder) { }
  numeroCuenta : string = '';

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    this.numeroCuenta = routeParams.get('cuenta') || '';
    this.formTransaction.reset({
      numeroCuenta: this.numeroCuenta,
    })
  }

  onSubmit() {
    const { numeroCuenta,monto, terminal,tipo } = this.formTransaction.value;
    this.bankService.createTransaction(numeroCuenta, terminal, tipo, monto)
        .subscribe( resp => {
          this.formTransaction.reset()
          Swal.fire({
            icon: resp.status ? 'success' : 'error',
            title: resp.message,
            showConfirmButton: false,
            timer: 1500
          })
        })
  }

}
