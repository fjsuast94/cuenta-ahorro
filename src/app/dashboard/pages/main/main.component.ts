import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styles: [
  ]
})
export class MainComponent implements OnInit {

  constructor(private authService: AuthService) {}
  user!: User;

  ngOnInit(): void {
    this.user = this.authService.user;
  }

  logout() {
    console.log("Funciona")
    this.authService.logout()
  }
}
