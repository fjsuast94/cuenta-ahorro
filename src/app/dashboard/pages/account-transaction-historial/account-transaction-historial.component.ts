import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TransactionResponse } from 'src/app/interfaces/account';
import { BankService } from 'src/app/services/bank.service';

@Component({
  selector: 'app-account-transaction-historial',
  templateUrl: './account-transaction-historial.component.html',
  styles: [
  ]
})
export class AccountTransactionHistorialComponent implements OnInit {

  constructor(
    private bankService: BankService,
    private route: ActivatedRoute,
  ) { }

  numeroCuenta : string = '';

  historial : Array<TransactionResponse> = []
  total : number = 0.0;

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    this.numeroCuenta = routeParams.get('cuenta') || '';

    this.bankService.readTransactions()
        .subscribe(resp => {
          if( resp.status ) {
            this.historial = resp.data
            this.historial.forEach( item => {
              if( item.tipo == 'Deposito') {
                this.total = parseFloat( (this.total + item.monto!).toString());
              } else {
                this.total = parseFloat((this.total - item.monto!).toString());
              }
            })

          }
        })
  }

}
