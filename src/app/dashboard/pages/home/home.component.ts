import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  private scripts = ['./assets/js/vendors.js', './assets/js/app.js'];

  constructor() { }

  ngOnInit(): void {
    this.addScriptJs(this.scripts)
  }

  ngOnDestroy(): void {
    //this.deleteScripJs(this.scripts)
  }


   addScriptJs(scripts :Array<string> = []){
    scripts.forEach(script => {
      const tag = document.createElement('script');
      tag.setAttribute('src', script);
      tag.setAttribute('type', 'text/javascript');
      document.body.appendChild(tag);
    });
  };

   deleteScripJs(scripts :Array<string> = []){
    scripts.forEach(script => {
      const tag = document.querySelector("script[src='" + script + "']");
      if (tag) {
        tag.remove();
      }
    });
  };

}
