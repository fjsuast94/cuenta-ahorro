import { Component, OnInit, OnDestroy } from '@angular/core';
import { AccountResponse } from 'src/app/interfaces/account';
import { BankService } from 'src/app/services/bank.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styles: [
  ]
})
export class AccountComponent implements OnInit {


  private scripts = ['./assets/js/vendors.js', './assets/js/app.js'];
  constructor(private bankService: BankService) { }

  accounts : Array<AccountResponse> = [];

  ngOnInit(): void {
    this.bankService.readAccount().subscribe( resp => {      
        if( resp.status) {
          this.accounts = resp.data
        }
    })
    //this.addScriptJs(this.scripts)
  }

  ngOnDestroy(): void {
    //this.deleteScripJs(this.scripts)
  }


   addScriptJs(scripts :Array<string> = []){
    scripts.forEach(script => {
      const tag = document.createElement('script');
      tag.setAttribute('src', script);
      tag.setAttribute('type', 'text/javascript');
      document.body.appendChild(tag);
    });
  };

   deleteScripJs(scripts :Array<string> = []){
    scripts.forEach(script => {
      const tag = document.querySelector("script[src='" + script + "']");
      if (tag) {
        tag.remove();
      }
    });
  };

}
