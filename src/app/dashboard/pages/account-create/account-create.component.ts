import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Client } from 'src/app/interfaces/client';
import { BankService } from 'src/app/services/bank.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-account-create',
  templateUrl: './account-create.component.html',
  styles: [
  ]
})
export class AccountCreateComponent implements OnInit {

  formAccount = this.fb.group({
    numeroCuenta: ['', [Validators.required]],
    saldoActual: [0.0, [Validators.required]],
    idClient: ['', [Validators.required]]
  });

  clients : Array<Client> = [];

  constructor( private fb: FormBuilder, private bank: BankService ) { }

  ngOnInit(): void {
    this.clients = this.bank.readClient();
  }


  onSubmit(): void {
      const { numeroCuenta, idClient, saldoActual } = this.formAccount.value
      this.bank.createAccount(numeroCuenta, parseInt(idClient), parseFloat(saldoActual))
      .subscribe( resp => {
        this.formAccount.reset()
        Swal.fire({
          icon: resp.status ? 'success' : 'error',
          title: resp.message,
          showConfirmButton: false,
          timer: 1500
        })
      })
  }

}
