import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountCreateComponent } from './pages/account-create/account-create.component';
import { AccountTransactionHistorialComponent } from './pages/account-transaction-historial/account-transaction-historial.component';
import { AccountTransactionComponent } from './pages/account-transaction/account-transaction.component';
import { AccountComponent } from './pages/account/account.component';
import { ClientCreateComponent } from './pages/client-create/client-create.component';
import { ClientComponent } from './pages/client/client.component';
import { HomeComponent } from './pages/home/home.component';
import { MainComponent } from './pages/main/main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'clientes',
        children: [
          {
            path: '',
            component: ClientComponent,
          },
          {
            path: 'crear',
            component: ClientCreateComponent,
          },
        ]
      },
      {
        path: 'cuentas',
        children: [
          {
            path: '',
            component: AccountComponent,
          },
          {
            path: 'crear',
            component: AccountCreateComponent,
          },
          {
            path: 'transaccion/:cuenta',
            component: AccountTransactionComponent,
          },
          {
            path: 'historial/:cuenta',
            component: AccountTransactionHistorialComponent,
          },
        ],
      },
      { path: "**", redirectTo: "clientes"}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
