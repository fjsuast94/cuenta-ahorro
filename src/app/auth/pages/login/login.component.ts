import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]],
  });

  constructor( private fb: FormBuilder, private authService: AuthService, private router: Router) { }
  ngOnInit(): void {}

  isValidFiel(field: string) {
    return this.loginForm.controls[field].errors && this.loginForm.controls[field].touched;
  }

  // get controls from form builder
  get f () {
    return this.loginForm.controls;  
  }


  onSubmit() {
    const { email, password } = this.loginForm.value;
    this.authService.login(email, password)
        .subscribe(resp => {
          if(!resp.status ) {
            Swal.fire(
              'Error',
              resp.message,
              'error'
            )
            return;
          }
          this.loginForm.reset()
          this.router.navigateByUrl('/dashboard')
        })
  }

}
