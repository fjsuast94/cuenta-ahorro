export interface AuthResponse {
    email? : string,
    displayName? : string,
    idToken?: string,
    registered?: string,
    refreshToken? : string,
    expiresIn? : string,
    error?: any,
}