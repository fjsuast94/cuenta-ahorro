
export interface Client {
  uuid?: number,
  nombre?: string,
  direccion?: string,
  edad?: number,
  genero?: string,
}
