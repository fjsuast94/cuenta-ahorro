
export interface User {
  name?: string,
  email?: string,
  uuid?: string,
  token?: string,
}


export interface TokenStorage {
  email?: string,
  name?: string,
  exp?: number
}
