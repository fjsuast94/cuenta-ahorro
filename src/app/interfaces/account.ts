export interface Account {
  name?: string,
}

export interface AccountResponse {
  estado?: string,
  fechaUltimaAct?: string,
  idCliente?: number,
  numeroCuenta?: string,
  saldo?: number
}
export interface TransactionResponse {
  fechaUltimaAct?: string,
  monto?: number
  numeroCuenta?: string,
  terminal?: string,
  tipo?: string
  usuario?: string,
}
