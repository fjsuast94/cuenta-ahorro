export interface HttpRespInterface {
    status: boolean,
    message?: string,
    data?: any
}