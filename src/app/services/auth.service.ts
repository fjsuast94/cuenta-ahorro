import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { AuthResponse } from '../interfaces/auth-interface';
import { tap, map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { TokenStorage, User } from '../interfaces/user';
import jwt_decode from "jwt-decode";
import { Router } from '@angular/router';
import { HttpRespInterface } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private authUrl : string = environment.authUrl;
  private _user! : User;

  get user() {
    return { ...this._user }
  }

  constructor(private http: HttpClient, private router: Router) {}

  login( email:string, password: string ): Observable<HttpRespInterface>  {
    const body = { email, password, returnSecureToken: true };
    const response : HttpRespInterface = { status: false, message: '', data: null } 
    return this.http.post(this.authUrl, body).pipe(
      tap( (resp : AuthResponse)  => {
        const { displayName, email, idToken } = resp
        localStorage.setItem('token', idToken!);
        this._user = {
          name: displayName,
          email,
          token: idToken
        }
      }),
      map( (resp : AuthResponse)   => {
        if( resp.idToken == null) {
          response.message = "El token no es valido";
          return response;
        }
        response.status = true;
        response.data = resp;
        return response;
      }),
      catchError( err => {
        response.message = this.getMessage(err.error?.error?.message)
        return of(response)
      })
    )
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigateByUrl('/auth')
  }

  getUserByToken() : Observable<boolean> {
    const tokenStorage = localStorage.getItem('token') || ""
    if( tokenStorage === "") {
      return of(false);
    }

    const isExpired = this.expiredToken(tokenStorage);

    if(!isExpired) {
      return of(false);
    }

    const token = jwt_decode<TokenStorage>(tokenStorage);
    this._user = {
      name: "",
      email: token.email,
      token: tokenStorage
    }
    return of(true);
  }

  getMessage(message: string ) : string  {
    const errorMessage  = {
      "EMAIL_NOT_FOUND": "El correo proporcionado no se encuentra regitrado",
      "INVALID_PASSWORD": "La contraseña es incorrecta",
    }
    const keyTyped = message as keyof typeof errorMessage;
    return errorMessage[keyTyped] ?? "Error desconocido";
  }

   expiredToken(token: string) : boolean {
    try {
      const decodedToken = jwt_decode<TokenStorage>(token);
      return decodedToken && (decodedToken.exp! * 1000) > new Date().getTime();
    } catch (error) {
      return false;
    }
  }
}
