import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";

import { Client } from '../interfaces/client';
import { Account, AccountResponse, TransactionResponse } from '../interfaces/account';
import { tap, map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import jwt_decode from "jwt-decode";
import { AuthResponse } from '../interfaces/auth-interface';
import { HttpRespInterface } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class BankService {

  private baseUrl : string = environment.baseUrl;
  constructor(private http: HttpClient) {}

  readClient() : Array<Client> {
    const clients : Array<Client> = JSON.parse(localStorage.getItem("clients") || "[]");
    return clients;
  }

  createClient(client : Client ) {
    const clients : Array<Client> = JSON.parse(localStorage.getItem("clients") || "[]");
    clients.push(client);
    localStorage.setItem("clients", JSON.stringify(clients));
  }

  readAccount(): Observable<HttpRespInterface> {
    const token = localStorage.getItem('token') || ''
    const response : HttpRespInterface = { status: false, message: '', data: null } 
    const url = `${this.baseUrl}/cuentaAhorro/OcBMnUGvAqVlUOskPph6ZIDpDqj2.json?auth=${token}`
    return this.http.get(url).pipe(
      map( (resp: any ) => {
        response.status = true;
        const accountsList : Array<string> = JSON.parse(localStorage.getItem("accountsList") || "[]");
        const accounts  : Array<AccountResponse> = []
        if( accountsList.length == 0 ) {
          response.data = accounts;
          return response;
        }
        accountsList.forEach( item => {
          for (const key in resp) {
            const keys: AccountResponse  = resp[item];
            if( key === item ) {
              accounts.push({
                "estado": keys["estado"],
                "fechaUltimaAct": keys["fechaUltimaAct"],
                "idCliente": keys["idCliente"],
                "numeroCuenta": keys["numeroCuenta"],
                "saldo": keys["saldo"]
              })
            }
          }
        })
        response.data = accounts;
        return response;
      }),
      catchError( err => {
        response.message = err.error?.error?.message;
        return of(response)
      })
    );
  }


  createAccount(numberAccount: string, idClient: number, saldo: number): Observable<HttpRespInterface> {
    const token = localStorage.getItem('token') || ''
    const url = `${this.baseUrl}/cuentaAhorro/OcBMnUGvAqVlUOskPph6ZIDpDqj2.json?auth=${token}`

    const body = {
      estado : "Activa",
      fechaUltimaAct: "2021-11-29",
      idCliente: idClient,
      numeroCuenta: numberAccount,
      saldo: saldo
    }
    const response : HttpRespInterface = { status: false, message: '', data: null } 
    return this.http.post(url, body).pipe(
      tap( (resp: Account) => {
        if( resp.name !== '') {
          const accountsList : Array<string> = JSON.parse(localStorage.getItem("accountsList") || "[]");
          accountsList.push(resp.name!);
          localStorage.setItem("accountsList", JSON.stringify(accountsList))
        }
      }),
      map( (resp: Account) => {
        response.status = resp.name !== ''
        response.message = response.status ? 'Se ha generado una cuenta nuevo' : '';
        return response;
      }),
      catchError( err => {
        response.message = err.error.error.message;
        return of(response);
      })
    );
  }


  readTransactions(): Observable<HttpRespInterface> {
    const token = localStorage.getItem('token') || ''
    const url = `${this.baseUrl}/transacciones/OcBMnUGvAqVlUOskPph6ZIDpDqj2.json?auth=${token}`
    const response : HttpRespInterface = { status: false, message: '', data: null } 

    return this.http.get(url).pipe(
      map( (resp: any) => {
        response.status = true
        const transactionsList : Array<string> = JSON.parse(localStorage.getItem("transactionsList") || "[]");
        const accounts  : Array<TransactionResponse> = []
        if( transactionsList.length == 0 ) {
          response.data = accounts
          return response
        }
        transactionsList.forEach( item => {
          for (const key in resp) {
            const keys = resp[key];
            if( key === item ) {
              accounts.push({
                "fechaUltimaAct": keys["fechaUltimaAct"],
                "monto": keys["monto"],
                "numeroCuenta": keys["numeroCuenta"],
                "terminal": keys["terminal"],
                "tipo": keys["tipo"],
                "usuario": keys["usuario"],
              })
            }
          }
        })
        return response
      }),
      catchError( err => {
        response.message = err.error?.error?.message
        return of(response)
      })
    )
  }


  createTransaction(numberAccount: string, terminal: string, tipo: string, monto: number) : Observable<HttpRespInterface> {
    const token = localStorage.getItem('token') || ''
    const { email } = jwt_decode<AuthResponse>(token)
    const url = `${this.baseUrl}/transacciones/OcBMnUGvAqVlUOskPph6ZIDpDqj2.json?auth=${token}`

    const body = {
      estado : "Activa",
      fechaUltimaAct: new Date(),
      terminal: terminal,
      numeroCuenta: numberAccount,
      monto: monto,
      tipo: tipo,
      usuario: email,
    }
    const response : HttpRespInterface = { status: false, message: '', data: null } 
    return this.http.post(url, body).pipe(
      tap( (resp: Account) => {
        if( resp.name !== '') {
          const transactionsList : Array<string> = JSON.parse(localStorage.getItem("transactionsList") || "[]");
          transactionsList.push(resp.name!);
          localStorage.setItem("transactionsList", JSON.stringify(transactionsList))
        }
      }),
      map( (resp: Account) => {
        response.status = resp.name !== '';
        response.message = response.status ? "Se ha generado una nueva transacción": ''
        return response;
      }),
      catchError( err => {
        response.message =err.error?.error?.message
        return of(response);
      })
    );
  }

}
